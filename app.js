const express = require('express')
const { stat } = require('fs')
const app = express()
const path = require('path')

//settting

// midlewares
app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static('./views'))


//routes
app.get('*', (req, res) => {
  res.send('Bienvenido')
})

app.listen(3000, () => {
  console.log(`Aplicación corriendo en el puerto 3000`)
})